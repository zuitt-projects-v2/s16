console.log("Hello World");

//While Loop
	/*
		~ A while loop takes in expression/condition

		Syntax: 
			while (expression/condition) {
				statement
			}
	*/
/*
let count = 5;

while (count !== 0 ) {
	console.log("While: " + count);
	count--;
}
*/
/*
	Mini activity:
		The while loop should only display the numbers from 1 - 5 or 5 - 1
		Correct the ff loop:

		let x = 0;

		while (x<1) {
			console.log(x);
			x--
		}

*/
/*
let x = 1;

while(x <= 5) {
	console.log(x);
	x++;
}

*/

//Do While Loop
	/*
		Syntax: 
			do {
				statement
			} while(expression/condition)

	*/
/*
let number = Number(prompt("Give me a number: "));

do {
	console.log("Do while: " + number);
	number += 1;
} while(number < 10)

*/

//For Loop

	/*
		Syntax: 
			for (intialization; expression/condition; finalExpression){
				statement
			}
		
	*/
/*
for (let count = 0; count <= 20; count++) {
	console.log("For Loop: " + count)
}


let myString = "Alex";
console.log(myString.length); //4
console.log(myString[0]);
console.log(myString[3]);  

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x])
}

*/

let myName = "AlEx";

for (let i=0; i < myName.length; i++) {

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		) {
		console.log("Vowel");
	} else {
		console.log(myName[i])
	}

}


//For Loop (Continue and Break statement)
/*
	Break statement is used to terminate the current loop once the match has been found

	Continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.

*/

for (let count = 0; count <= 20; count++) {

	console.log("Hello")

	if (count % 2 === 0) {
		console.log("Even Number")
		continue;
	}

	console.log("Continue and Break: " + count)

	if(count > 10) {
		break;
	}

}

let name = "alexandro";

for(let i = 0; i < name.length; i++) {
	console.log(name[i])

	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	}

	if (name[i].toLowerCase() === "d") {
		break;
	}

}
